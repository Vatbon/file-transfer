package fileprotocol;

import util.MD5Hash;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ProtocolPacketFactory {
    private final File file;
    private FileInputStream fileInputStream;

    public ProtocolPacketFactory(File file) throws FileNotFoundException {
        this.file = file;
        this.fileInputStream = new FileInputStream(file);
    }

    public byte[] build(Protocol.PackageType type) {

        switch (type) {
            case PING:
                return new byte[]{0, 0, 0, 0, 0};
            case START:
                byte[] filename = StandardCharsets.UTF_8.encode(file.getName()).array();
                return ByteBuffer.allocate(1 + 4 + filename.length).put(Protocol.getByteFromPackageType(type))
                        .putInt(filename.length)
                        .put(filename)
                        .array();
            case TRANSMISSION:
                try {
                    byte[] buff = new byte[131072];
                    int read = fileInputStream.read(buff);
                    if (read == -1)
                        throw new EOFException();
                    return ByteBuffer.allocate(1 + 4 + read)
                            .put(Protocol.getByteFromPackageType(type))
                            .putInt(read).put(buff, 0, read)
                            .array();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case END:
                try {
                    byte[] hash = MD5Hash.createChecksum(file);
                    return ByteBuffer.allocate(1 + 4 + hash.length).put(Protocol.getByteFromPackageType(type))
                            .putInt(hash.length)
                            .put(hash)
                            .array();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                return new byte[]{};
        }
        return new byte[]{};
    }

    public boolean isFileEOF() {
        try {
            if (fileInputStream.available() == 0)
                return true;
        } catch (IOException e) {
            e.printStackTrace();
            return true;
        }
        return false;
    }
}
