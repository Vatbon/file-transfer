package fileprotocol;

public class Protocol {

    public enum PackageType {
        START, TRANSMISSION, END, PING;
    }

    static final int START = 1;
    static final int TRANSMISSION = 2;
    static final int END = 3;
    static final int PING = 0;

    public static byte getByteFromPackageType(PackageType type) {
        switch (type) {
            case START:
                return START;
            case TRANSMISSION:
                return TRANSMISSION;
            case END:
                return END;
            case PING:
                return PING;
            default:
                return -1;
        }
    }

    public static PackageType getPackageTypeFromByte(byte code) {
        switch (code) {
            case START:
                return PackageType.START;
            case TRANSMISSION:
                return PackageType.TRANSMISSION;
            case END:
                return PackageType.END;
            case PING:
                return PackageType.PING;
            default:
                return PackageType.PING;
        }
    }
}
