package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Server {

    static final int CONNECTION_TIMEOUT = 10000;
    static final String UPLOADS_PATH = "uploads/";
    static final int TIMEOUT = 3000; // in milliseconds
    private static long instant;
    private static ServerSocket serverSocket;
    private static List<Connection> connections;

    public static void run(int port) {
        try {
            Files.createDirectories(Paths.get(UPLOADS_PATH));
            serverSocket = new ServerSocket(port);
            connections = new ArrayList<>();
            instant = System.currentTimeMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            serverSocket.setSoTimeout(TIMEOUT / 3);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        instant = System.currentTimeMillis();
        boolean quit = false;
        while (!quit) {
            try {
                Socket socket = serverSocket.accept();
                connections.add(new Connection(socket));
                printSpeed();
            } catch (SocketTimeoutException e) {
                printSpeed();
            } catch (IOException e) {
                closeWithResources();
                e.printStackTrace();
            }

            /*Read console to find word 'quit' and exit the program*/
            try {
                if (System.in.available() > 4) {
                    String input = new String(System.in.readNBytes(4));
                    if ("quit".equals(input)) {
                        System.out.println("Exit program.");
                        quit = true;
                    }
                }
            } catch (IOException ignored) {
            }
        }
        closeWithResources();
    }

    private static void closeWithResources() {
        for (Connection connection : connections) {
            try {
                connection.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            connection.close();
        }
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printSpeed() {
        if (System.currentTimeMillis() - instant > TIMEOUT) {
            for (Connection connection : connections) {
                if (connection != null && connection.isAlive() && connection.getFile() != null) {
                    System.out.println("\033[0;33m\"" + connection.getFile().getName() + "\"\033[0m:");
                    System.out.println("- \033[0;36m" + connection.getIpAddress() + "\033[0m" + "'s average speed: \033[0;32m" + connection.getAverageSpeed() / 1024 + "\033[0m" + " KB/s");
                    System.out.println("- \033[0;36m" + connection.getIpAddress() + "\033[0m" + "'s current speed: \033[0;32m" + connection.getCurrentSpeed() / 1024 + "\033[0m" + " KB/s");
                }
            }
            instant = System.currentTimeMillis();
        }
    }
}
