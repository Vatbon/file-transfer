package server;

import fileprotocol.Protocol;
import util.MD5Hash;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Connection extends Thread {

    private final Socket socket;
    private File file;
    private FileOutputStream fos;
    private long start;
    private long step;
    private long sum;
    private long current;

    public Connection(Socket socket) {
        this.socket = socket;
        this.start();
        current = sum = 0;
    }

    @Override
    public void run() {
        try (InputStream is = socket.getInputStream()
        ) {
            boolean readable = true;
            long lastTimeReceived = step = start = System.currentTimeMillis();
            byte[] head = new byte[5];
            while (readable) {
                if (System.currentTimeMillis() - step > Server.TIMEOUT) {//every 3 sec
                    current = sum;
                    step = System.currentTimeMillis();
                }

                if (System.currentTimeMillis() - lastTimeReceived > Server.CONNECTION_TIMEOUT) {
                    System.out.println("\033[1;33m# \033[0;36m" + this.getIpAddress() + "\033[0m's connection is lost.");
                    this.close();
                    return;
                }

                byte empty = -1;
                head[0] = empty;
                if (is.available() >= 5) {
                    is.read(head);
                    lastTimeReceived = System.currentTimeMillis();
                }
                switch (Protocol.getPackageTypeFromByte(head[0])) {
                    case START:
                        int fileNameLength = ByteBuffer.wrap(head, 1, 4).getInt();
                        String fileName = String.valueOf(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(is.readNBytes(fileNameLength))));
                        sum += 5 + fileNameLength;
                        file = new File((Server.UPLOADS_PATH + fileName).trim());
                        if (!file.exists()) {
                            file.createNewFile();
                        } else {
                            System.out.println("File \033[0;33m\"" + fileName + "\"\033[0m already exists.");
                            socket.close();
                            return;
                        }
                        System.out.println("\033[1;33m# \033[0;36m" + socket.getInetAddress().getHostAddress() + "\033[0m started uploading \033[0;33m\"" + fileName.trim() + "\"\033[0m");
                        fos = new FileOutputStream(file);
                        break;

                    case TRANSMISSION:
                        int messageLength = ByteBuffer.wrap(head, 1, 4).getInt();
                        sum += 5 + messageLength;
                        fos.write(is.readNBytes(messageLength));
                        break;

                    case END:
                        readable = false;
                        int hashLength = ByteBuffer.wrap(head, 1, 4).getInt();
                        byte[] hashClient = is.readNBytes(hashLength);
                        sum += 5 + hashLength;

                        /*Final info about transmission*/
                        boolean good = Arrays.equals(hashClient, MD5Hash.createChecksum(file));
                        synchronized (System.in) {
                            if (good)
                                System.out.println("\033[1;33m#\033[0;33m\"" + file.getName() + "\"\033[0m is \033[0;32mgood\033[0m");
                            else
                                System.out.println("\033[1;33m#\033[0;33m\"" + file.getName() + "\"\033[0m is \033[0;31moh-oh\033[0m");
                            long finish = System.currentTimeMillis();
                            System.out.println("\033[1;33m#\033[0m \033[0;36m" + this.getIpAddress() + "\033[0m" + "'s average speed: \033[0;32m" + this.getAverageSpeed() / 1024 + "\033[0m" + " KB/s");
                            System.out.println("\033[1;33m#\033[0m Transmission took \033[0;32m" + String.format("%.2f", (finish - start) / 1000.0) + "\033[0m sec");
                            System.out.println("\033[1;33m#\033[0m Size of the file: \033[0;32m" + String.format("%.2f", file.length() / 1024.0) + "\033[0m KB");
                        }
                        break;

                    case PING:
                        break;

                    default:
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            this.close();
        }
    }

    public long getCurrentSpeed() {
        return (sum - current) / (System.currentTimeMillis() - step) * 1000;
    }

    public long getAverageSpeed() {
        return sum / (System.currentTimeMillis() - start) * 1000;
    }

    public String getIpAddress() {
        return socket.getInetAddress().getHostAddress();
    }

    public File getFile() {
        return file;
    }

    public void close() {
        try {
            if (socket != null)
                socket.close();
            if (fos != null)
                fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
