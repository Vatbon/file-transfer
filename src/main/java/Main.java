import client.Client;
import server.Server;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {
    private static final String CLIENT_MODE = "client";
    private static final String SERVER_MODE = "server";
    private static final String PORT_ARGUMENT = "-p";
    private static final String IP_ARGUMENT = "-i";
    private static final String FILE_ARGUMENT = "-f";
    private static final String MODE_ARGUMENT = "-m";
    private static final String MAN = "-m {client, server}\n" +
            "-p {port}\n" +
            "-i {IP address}\n" +
            "-f {filePath}";

    public static void main(String[] args) {
        Map<String, String> options = parseArgs(args);

        if (!options.containsKey(MODE_ARGUMENT)) {
            System.out.println("No mode entered.");
            System.out.println(MAN);
            System.exit(1);
        }

        if (options.get(MODE_ARGUMENT).equals(CLIENT_MODE)) {
            try {
                if (!options.containsKey(IP_ARGUMENT)) {
                    System.out.println("Missing IP argument.");
                    System.out.println(MAN);
                    System.exit(1);
                }
                if (!options.containsKey(PORT_ARGUMENT)) {
                    System.out.println("Missing port argument.");
                    System.out.println(MAN);
                    System.exit(1);
                }
                if (!options.containsKey(FILE_ARGUMENT)) {
                    System.out.println("Missing file argument.");
                    System.out.println(MAN);
                    System.exit(1);
                }
                new Client().run(options.get(IP_ARGUMENT), Integer.parseInt(options.get(PORT_ARGUMENT)), options.get(FILE_ARGUMENT));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (options.get(MODE_ARGUMENT).equals(SERVER_MODE)) {
            if (!options.containsKey(PORT_ARGUMENT)) {
                System.out.println("Missing port argument.");
                System.out.println(MAN);
                System.exit(1);
            }
            Server.run(Integer.parseInt(options.get(PORT_ARGUMENT)));
        } else
            System.out.println("Wrong mode entered.");
    }

    private static Map<String, String> parseArgs(String[] args) {
        HashMap<String, String> options = new HashMap<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(PORT_ARGUMENT)) {//port
                options.put(PORT_ARGUMENT, args[i + 1]);
            }
            if (args[i].equals(IP_ARGUMENT)) {//ip
                options.put(IP_ARGUMENT, args[i + 1]);
            }
            if (args[i].equals(FILE_ARGUMENT)) {//file
                options.put(FILE_ARGUMENT, args[i + 1]);
            }
            if (args[i].equals(MODE_ARGUMENT)) {//mode
                options.put(MODE_ARGUMENT, args[i + 1]);
            }
        }
        return options;
    }

}
