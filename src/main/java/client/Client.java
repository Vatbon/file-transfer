package client;

import client.customsocket.FileSocket;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.SocketException;

public class Client {
    public void run(String ip, int port, String filePath) throws IOException {
        /*File parsing*/
        File file = new File(filePath);
        if (!file.canRead()) {
            System.out.println("\033[0;31mCan read file\033[0m");
            System.exit(1);
        }
        String fileName = file.getName();
        if (!file.isFile()) {
            System.out.println("\033[0;31m" + fileName + " is not a file\033[0m");
            System.exit(1);
        }
        System.out.println("File : \033[0;33m\"" + fileName + "\"\033[0m");
        System.out.println("Size : \033[0;32m" + String.format("%.2f", file.length() / 1024.0) + "\033[0m KB");

        try {
            /*Creating socket*/
            InetAddress inetAddress = InetAddress.getByName(ip);
            FileSocket fileSocket = new FileSocket(inetAddress, port);
            System.out.println("Server address: \033[0;36m" + fileSocket.getInetAddress().getCanonicalHostName() + "\033[0m");
            System.out.println("Client address: \033[0;36m" + fileSocket.getLocalAddress().getCanonicalHostName() + "\033[0m");
            /*Transmission*/
            fileSocket.send(file);
            fileSocket.close();
            System.out.println("Transmission is over");
        } catch (ConnectException e) {
            System.out.println("Connection to \033[0;33m" + ip + ":" + port + "\033[0m is \033[0;31mrefused.\033[0m");
            System.exit(1);
        } catch (SocketException e) {
            System.out.println("Unable to connect to \033[0;36m" + ip + ":" + port + "\033[0m");
        }
    }
}
