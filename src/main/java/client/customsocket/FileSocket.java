package client.customsocket;

import fileprotocol.Protocol;
import fileprotocol.ProtocolPacketFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class FileSocket {
    private Socket socket;

    public FileSocket(InetAddress inetAddress, int port) throws IOException {
        socket = new Socket(inetAddress, port);
    }

    public InetAddress getInetAddress() {
        return socket.getInetAddress();
    }

    public InetAddress getLocalAddress() {
        return socket.getLocalAddress();
    }

    public void send(File file) {
        boolean quit = false;
        try {
            ProtocolPacketFactory protocolPacketFactory = new ProtocolPacketFactory(file);
            socket.setSoTimeout(10000);
            OutputStream os = socket.getOutputStream();
            /*Start*/
            byte[] packet = protocolPacketFactory.build(Protocol.PackageType.START);
            os.write(packet);
            os.flush();

            /*Middle*/
            while (!protocolPacketFactory.isFileEOF()) {
                try {
                    if (System.in.available() > 4) {
                        String input = new String(System.in.readNBytes(4));
                        if ("quit".equals(input)) {
                            System.out.println("Exit program.");
                            this.close();
                            return;
                        }
                    }
                } catch (IOException ignored) {
                }
                packet = protocolPacketFactory.build(Protocol.PackageType.TRANSMISSION);
                os.write(packet);
                os.flush();
            }

            /*End*/
            packet = protocolPacketFactory.build(Protocol.PackageType.END);
            os.write(packet);
            os.flush();
        } catch (SocketTimeoutException e) {
            System.out.println("Connection timed out");
        } catch (SocketException e) {
            System.out.println("Connection reset by peer");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            this.close();
        }
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
